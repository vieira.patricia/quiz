import { Component, OnInit } from '@angular/core';
import { Question } from '../model/question';
import { QuestionService } from '../question.service';
import { QuizRequestDto } from '../model/requestDto';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  questions: Question[];

  requestDtoArray = [];
  
  constructor(private questionService: QuestionService) { }

  ngOnInit() {
    this.questionService.getQuestions().subscribe(data => {
      this.questions = data;
    });
  }

  answerQuestion(question, answerSelected) {

    const requestDtoObj = {QuizRequestDto};
    requestDtoObj.questionId = question.id;
    requestDtoObj.answerId = answerSelected.id;

    // Check if the question has been answered before
    const foundQuestion = this.requestDtoArray.find(x => x.questionId === question.id);
   
    if (foundQuestion !== undefined) {
      // Identify the question index
      const index = this.requestDtoArray.indexOf(this.requestDtoArray.find(x => x.questionId === question.id));
      // Remove the question answered from array
      this.requestDtoArray.splice(index, 1);
      // Add the question with a different answer
      this.requestDtoArray.push(requestDtoObj);
    }
    
    // In case the question never has been answered before
    if (foundQuestion === undefined) {
      // Add a question into the array
       this.requestDtoArray.push(requestDtoObj);
    }
  }

  submit(): void {
    this.questionService.submit(this.requestDtoArray)
      .subscribe( (data) => {
        alert('Thank you for completing the QUIZ! SCORE:  ' + data.score);
        console.log(data);
      });
  }
}
