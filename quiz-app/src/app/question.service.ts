import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Question } from './model/question';
import { QuestionRequestDto } from './model/requestDto';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  private quizUrl = 'http://localhost:8080/quiz-app';
  

  constructor(private http: HttpClient) {
  }

  public getQuestions() {
    return this.http.get<Question[]>(this.quizUrl + '/questions');
  }

  public submit(questionRequestDto) {
    return this.http.post<QuestionRequestDto >(this.quizUrl + '/submit', questionRequestDto);
  }
}
