package com.competa.quiz.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.competa.quiz.dto.QuizRequestDto;
import com.competa.quiz.dto.QuestionResponseDto;
import com.competa.quiz.dto.QuizResponseDto;
import com.competa.quiz.service.QuizService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping({"/quiz-app"})
public class QuizController {

	private QuizService quizService;

	public QuizController(QuizService quizService) {
		this.quizService = quizService;
	}

	@RequestMapping(value = "/questions",  method = RequestMethod.GET)
	@ResponseBody
	public List<QuestionResponseDto> getQuestions() {
		return quizService.getQuestions();
	}

	@RequestMapping(value = "/submit",  method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public QuizResponseDto subnit(@RequestBody List<QuizRequestDto> questionRequestDto) {
		return quizService.calculateScore(questionRequestDto);
	}
}
