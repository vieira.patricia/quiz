package com.competa.quiz.service;

import org.springframework.stereotype.Component;

import com.competa.quiz.domain.Answer;
import com.competa.quiz.domain.Question;
import com.competa.quiz.repository.AnswerRepository;
import com.competa.quiz.repository.QuestionRepository;

/**
 * Class to populate the database with questions/answers
 *
 */
@Component
public class LoadQuizImpl implements LoadQuiz {

	private QuestionRepository questionRepository;
	private AnswerRepository answerRepository;

	public LoadQuizImpl(QuestionRepository questionRepository, AnswerRepository answerRepository) {
		this.questionRepository = questionRepository;
		this.answerRepository = answerRepository;
	}

	@Override
	public void populateDatabase() {
		//QUESTION 1
		Question q1 = new Question("In what year was Google launched on the web?");
		q1 = questionRepository.save(q1);
		Answer answer = new Answer("1998", true);
		answer.setQuestion(q1);
		answerRepository.save(answer);
		answer = new Answer("1995", false);
		answer.setQuestion(q1);
		answerRepository.save(answer);
		answer = new Answer("1996", false);
		answer.setQuestion(q1);
		answerRepository.save(answer);
		answer = new Answer("1997", false);
		answer.setQuestion(q1);
		answerRepository.save(answer);

		//QUESTION 2
		Question q2 = new Question("What is the capital of Turkey?");
		q2 = questionRepository.save(q2);
		answer = new Answer("Ankara", true);
		answer.setQuestion(q2);
		answerRepository.save(answer);
		answer = new Answer("Konya", false);
		answer.setQuestion(q2);
		answerRepository.save(answer);
		answer = new Answer("Adana", false);
		answer.setQuestion(q2);
		answerRepository.save(answer);
		answer = new Answer("Bodrum", false);
		answer.setQuestion(q2);
		answerRepository.save(answer);

		//QUESTION 3
		Question q3 = new Question("How many continents are there?");
		questionRepository.save(q3);
		answer = new Answer("7", true);
		answer.setQuestion(q3);
		answerRepository.save(answer);
		answer = new Answer("6", false);
		answer.setQuestion(q3);
		answerRepository.save(answer);
		answer = new Answer("5", false);
		answer.setQuestion(q3);
		answerRepository.save(answer);
		answer = new Answer("4", false);
		answer.setQuestion(q3);
		answerRepository.save(answer);

		//QUESTION 4
		Question q4 = new Question("What is the second largest country in Europe after Russia?");
		questionRepository.save(q4);
		answer = new Answer("France", true);
		answer.setQuestion(q4);
		answerRepository.save(answer);
		answer = new Answer("Netherland", false);
		answer.setQuestion(q4);
		answerRepository.save(answer);
		answer = new Answer("Belgium", false);
		answer.setQuestion(q4);
		answerRepository.save(answer);
		answer = new Answer("Germany", false);
		answer.setQuestion(q4);
		answerRepository.save(answer);

		//QUESTION 4
		Question q5 = new Question("What is the name of the LLama in Fortinite?");
		questionRepository.save(q5);
		answer = new Answer("Loot llama", true);
		answer.setQuestion(q5);
		answerRepository.save(answer);
		answer = new Answer("Lucky llama", false);
		answer.setQuestion(q5);
		answerRepository.save(answer);
		answer = new Answer("Luck llama", false);
		answer.setQuestion(q5);
		answerRepository.save(answer);
		answer = new Answer("Upgrade llama", false);
		answer.setQuestion(q5);
		answerRepository.save(answer);
	}

}
