package com.competa.quiz.service;

import java.util.List;

import com.competa.quiz.dto.QuizRequestDto;
import com.competa.quiz.dto.QuestionResponseDto;
import com.competa.quiz.dto.QuizResponseDto;

public interface QuizService {
	
	/**
	 * Method to to show the questions and answers persisted in the database 
	 * @return - list containing all questions persisted
	 */
	List<QuestionResponseDto> getQuestions();
	/**
	 * Method to calculate score based on user answers
	 * @param questionRequestDtoList - contains a list of questions answered by the user
	 * @return - string containing the score classification 
	 */
	QuizResponseDto calculateScore(List<QuizRequestDto> questionRequestDtoList);
	
}
