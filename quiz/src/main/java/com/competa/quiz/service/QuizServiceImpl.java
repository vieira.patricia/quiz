package com.competa.quiz.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.competa.quiz.domain.Question;
import com.competa.quiz.dto.AnswerResponseDto;
import com.competa.quiz.dto.QuizRequestDto;
import com.competa.quiz.dto.QuestionResponseDto;
import com.competa.quiz.dto.QuizResponseDto;
import com.competa.quiz.repository.AnswerRepository;
import com.competa.quiz.repository.QuestionRepository;

@Component
public class QuizServiceImpl implements QuizService {

	private QuestionRepository questionRepository;
	private AnswerRepository answerRepository;
	private static final Logger LOGGER = LoggerFactory.getLogger(QuizServiceImpl.class);


	public QuizServiceImpl(QuestionRepository questionRepository, AnswerRepository answerRepository) {
		this.questionRepository = questionRepository;
		this.answerRepository = answerRepository;
	}

	@Override
	public QuizResponseDto calculateScore(List<QuizRequestDto> questionRequestDtoList) {
		LOGGER.info("Calculating the score");
		int totalQuestions = questionRepository.totalQuestions();
		List<Long> correctAnswers = getAllCorrectAnswers();
		int score = 0;
		for (QuizRequestDto request : questionRequestDtoList) {
			if (correctAnswers.contains(request.getAnswerId())) {
				score++;
			}
		}

		StringBuilder result = determineScoreClassification(totalQuestions, score);
		result.append(score).append("/").append(totalQuestions);

		QuizResponseDto resultDto = new QuizResponseDto(result.toString());

		return resultDto;
	}

	@Override
	public List<QuestionResponseDto> getQuestions() {
		List<Question> questions = findAllQuestions();
		
		return toQuestionResponseDto(questions);
	}
	
	/**
	 * Method to retrieve all question in the database
	 * @return a list of questions
	 */
	private List<Question> findAllQuestions() {
		LOGGER.info("Retrieving questions from the database");
		return questionRepository.findAll();
	}
	
	/**
	 * Method to retrieve all answers form the database
	 * @return a list of answers
	 */
	private List<Long> getAllCorrectAnswers() {
		LOGGER.info("Retrieving answers from the database");
		return answerRepository.getAllCorrectAnswers();
	}


	/**
	 * Method to determine the score classification based on the quantity of questions and the correct answers
	 * @param totalQuestions - total of questions
	 * @param totalCorrectAnswer - correct answers
	 * @return the string classification definition
	 */
	private static StringBuilder determineScoreClassification(int totalQuestions, int totalCorrectAnswer) {
		int calculateScore = (totalCorrectAnswer*100) / totalQuestions;
		StringBuilder scoreClassification = new StringBuilder();

		if (calculateScore < 25) {
			scoreClassification.append("Fair ");
		}else if (calculateScore > 25 && calculateScore <= 50) {
			scoreClassification.append("Good ");
		} else if (calculateScore > 50) {
			scoreClassification.append("Excellent ");
		}

		return scoreClassification;
	}

	/**
	 * Method to convert questions persisted in question response dto
	 * @param questions persisted
	 * @return a list containing questions response dto
	 */
	private List<QuestionResponseDto> toQuestionResponseDto(List<Question> questions) {
		LOGGER.info("Converting questions in question response dto");
		List<QuestionResponseDto> response = new ArrayList<>();

		for (Question question : questions) {			
			List<AnswerResponseDto> answerResponseList = question.getAnswers().stream()
					.map(a -> new AnswerResponseDto(a.getId(), a.getAnswer()))
					.collect(Collectors.toList());

			QuestionResponseDto questionResponseDto = new QuestionResponseDto(question.getId(), question.getQuestion(), answerResponseList);
			
			response.add(questionResponseDto);
			
		}
		
		return response;
	}

}
