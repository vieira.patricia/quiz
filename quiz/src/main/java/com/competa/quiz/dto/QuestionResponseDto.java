package com.competa.quiz.dto;

import java.util.List;

public class QuestionResponseDto {
	
	private long id;
	private String question;
	List<AnswerResponseDto> answerResponseDtos;
	
	public QuestionResponseDto(long id, String question, List<AnswerResponseDto> answerResponseDtos) {
		this.id = id;
		this.question = question;
		this.answerResponseDtos = answerResponseDtos;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public List<AnswerResponseDto> getAnswerResponseDtos() {
		return answerResponseDtos;
	}
	public void setAnswerResponseDtos(List<AnswerResponseDto> answerResponseDtos) {
		this.answerResponseDtos = answerResponseDtos;
	}
}
