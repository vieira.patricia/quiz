package com.competa.quiz.dto;

public class QuizResponseDto {

	private String score;

	public QuizResponseDto() {}

	public QuizResponseDto(String score) {
		this.score = score;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}
}
