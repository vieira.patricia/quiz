package com.competa.quiz.dto;

public class QuizRequestDto {

	private long questionId;
	private long answerId;
	
	public QuizRequestDto(long questionId, long answerId) {
		this.questionId = questionId;
		this.answerId = answerId;
	}
	public long getQuestionId() {
		return questionId;
	}
	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}
	public long getAnswerId() {
		return answerId;
	}
	public void setAnswerId(long answerId) {
		this.answerId = answerId;
	}
}
