package com.competa.quiz;


import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.competa.quiz.repository.AnswerRepository;
import com.competa.quiz.repository.QuestionRepository;
import com.competa.quiz.service.LoadQuiz;
import com.competa.quiz.service.LoadQuizImpl;

@SpringBootApplication
public class QuizApplication {

	@Autowired
	private QuestionRepository questionRepository;
	@Autowired
	private AnswerRepository answerRepository;

	public static void main(String[] args) {
		SpringApplication.run(QuizApplication.class, args);
	}

	@Bean
	InitializingBean sendDatabase() {
		return () -> {
			final LoadQuiz loadQuiz = new LoadQuizImpl(questionRepository, answerRepository);
			loadQuiz.populateDatabase();
		};
	}
}
