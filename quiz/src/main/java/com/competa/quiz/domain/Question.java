package com.competa.quiz.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Class to represent Question
 *
 */
@Entity
@Table(name = "question")
public class Question {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String question;
	@OneToMany(
			mappedBy = "question", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true
			)
	private List<Answer> answers = new ArrayList<>();
	
	public Question() {
	}
	
	public Question(String question) {
		this.question = question;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public List<Answer> getAnswers() {
		return answers;
	}
	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}
}
