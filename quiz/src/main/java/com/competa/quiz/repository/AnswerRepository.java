package com.competa.quiz.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.competa.quiz.domain.Answer;

public interface AnswerRepository extends JpaRepository<Answer, Long> {
	@Query("SELECT a.id FROM Answer a where isCorrect = true")
    List<Long> getAllCorrectAnswers();

}
