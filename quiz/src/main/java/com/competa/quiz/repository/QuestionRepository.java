package com.competa.quiz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.competa.quiz.domain.Question;

public interface QuestionRepository extends JpaRepository<Question, Long>{
	@Query("SELECT COUNT(q) FROM Question q")
    int totalQuestions();
}
