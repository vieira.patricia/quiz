package com.competa.quiz.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import com.competa.quiz.domain.Question;
import com.competa.quiz.dto.QuizRequestDto;
import com.competa.quiz.dto.QuizResponseDto;
import com.competa.quiz.repository.AnswerRepository;
import com.competa.quiz.repository.QuestionRepository;

@RunWith(SpringRunner.class)
public class QuizServiceTest {

	@Mock
	private QuestionRepository questionRepository;
	@Mock
	private QuizService quizService;
	@Mock
	private AnswerRepository answerRepository;

	List<Long> correctAnswers = new ArrayList<>();

	/**
	 * Method to simulate the decisions taken by the user
	 * @param questions
	 * @return
	 */
	private List<QuizRequestDto> createRequest() {
		long questionId = 1;
		long answerId = 2;
		QuizRequestDto questionRequestDto = new QuizRequestDto(questionId, answerId);
		List<QuizRequestDto> request = new ArrayList<>();
		request.add(questionRequestDto);
	
		return request;
	}

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		quizService = new QuizServiceImpl(questionRepository, answerRepository);
		when(questionRepository.findAll()).thenReturn(new ArrayList<Question>());
		when(questionRepository.totalQuestions()).thenReturn(5);
		when(answerRepository.getAllCorrectAnswers()).thenReturn(correctAnswers);
	}

	@Test
	public void calculateScoreTest() {
		QuizResponseDto result = quizService.calculateScore(createRequest());
		assertThat(result, is(instanceOf(QuizResponseDto.class)));
		verify(answerRepository, times(1)).getAllCorrectAnswers();
	}
	
	@Test
	public void showQuestionsTest() {
		quizService.getQuestions();
		verify(questionRepository, times(1)).findAll();
	}
}
